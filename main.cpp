#include "mainwindow.h"
#include <QApplication>

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
#include <QDir>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //MainWindow w;
    //w.show();

    QGraphicsScene scene;
    QGraphicsView view;
    QGraphicsRectItem* rect = new QGraphicsRectItem(0,0,20,100);
    QGraphicsRectItem* rect2 = new QGraphicsRectItem(200,100,20,100);
    scene.addItem(rect);
    scene.addItem(rect2);
    view.setScene(&scene);
    view.setFixedSize(640,320);
    view.setSceneRect(0,0,640,320);
    view.show();

    qDebug() << rect2->pos();
    QString text;
    QFile file( QDir::current().absolutePath() + "/debug/map/Map1.json");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        qDebug()<< "Erroro";
    text = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(text.toUtf8());
    QJsonObject obj = doc.object();
    //qDebug() << obj;
    QJsonArray test = obj["layers"].toArray();
    qDebug() << (test[0].toObject())["width"];



    return a.exec();
}
