#ifndef GAME_H
#define GAME_H
#include "maprenderer.h"

class Game
{
public:
    Game();

    void update();
    void draw();

};

#endif // GAME_H
