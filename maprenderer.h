#ifndef MAPRENDERER_H
#define MAPRENDERER_H
#include "tiles.h"

class MapRenderer
{
public:
    MapRenderer();

    void loadMap();
    void drawMap();


};

#endif // MAPRENDERER_H
