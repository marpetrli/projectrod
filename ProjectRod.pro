#-------------------------------------------------
#
# Project created by QtCreator 2016-02-07T23:39:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProjectRod
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    maprenderer.cpp \
    tiles.cpp \
    objects.cpp \
    game.cpp

HEADERS  += mainwindow.h \
    maprenderer.h \
    tiles.h \
    objects.h \
    game.h

FORMS    += mainwindow.ui

CONFIG += c++17

RESOURCES +=
